package org.nathan.cam;

import org.bukkit.plugin.java.*;
import org.bukkit.plugin.*;
import java.util.logging.*;
import org.bukkit.entity.*;
import org.bukkit.scheduler.*;
import org.nathan.cam.commands.FreecamCommand;
import org.nathan.cam.data.PlayerData;
import org.nathan.cam.datasqlite.SQLite;
import org.nathan.cam.listeners.PlayerListener;
import org.nathan.cam.utils.NPCUtils;
import org.bukkit.configuration.file.*;
import org.bukkit.event.*;
import java.util.*;
import org.bukkit.command.*;
import org.bukkit.*;

public class Freecam extends JavaPlugin
{
    private static Plugin plugin;
    public static PluginManager bpm;
    public static BukkitScheduler sch;
    public static Logger log;
    public static SQLite db;
    public static Map<Player, PlayerData> active_players;
    public static Map<Player, BukkitTask> active_tasks;
    public static FileConfiguration cfg;
    public static NPCUtils npc;
    
    static {
        Freecam.bpm = Bukkit.getPluginManager();
        Freecam.sch = Bukkit.getScheduler();
        Freecam.log = Bukkit.getLogger();
        Freecam.active_players = new HashMap<Player, PlayerData>();
        Freecam.active_tasks = new HashMap<Player, BukkitTask>();
        Freecam.npc = null;
    }
    
    public void onEnable() {
        ((Freecam)(Freecam.plugin = (Plugin)this)).saveResource("config.yml", false);
        Freecam.db = new SQLite(this);
        if (!Freecam.db.load()) {
            Freecam.log.severe("Could not connect to SQLite!");
            Freecam.bpm.disablePlugin((Plugin)this);
            return;
        }
        Freecam.db.get();
        for (final Player p : Bukkit.getOnlinePlayers()) {
            if (Freecam.active_players.containsKey(p)) {
                disableFreecam(p);
            }
        }
        this.getCommand("freecam").setExecutor((CommandExecutor)new FreecamCommand());
        Freecam.bpm.registerEvents((Listener)new PlayerListener(), Freecam.plugin);
        Freecam.cfg = this.getConfig();
        Freecam.sch.scheduleSyncRepeatingTask(Freecam.plugin, () -> Freecam.db.put(), 200L, 200L);
        if (Freecam.cfg.getBoolean("npcEnabled")) {
            if (Freecam.bpm.isPluginEnabled("Citizens")) {
                Freecam.npc = new NPCUtils();
                Freecam.log.info("Citizens Found - NPCs Enabled");
            }
            else {
                Freecam.log.info("Citizens could not be found - NPCs Disabled");
            }
        }
        Freecam.log.info("Freecam Enabled");
    }
    
    public void onDisable() {
        if (!Freecam.active_players.isEmpty()) {
            for (final Player p : Freecam.active_players.keySet()) {
                disableFreecam(p);
            }
        }
        Freecam.log.info("Freecam Disabled");
        Freecam.plugin = null;
    }
    
    public static void registerEvent(final Listener listener) {
        Freecam.bpm.registerEvents(listener, Freecam.plugin);
    }
    
    public static Plugin getPlugin() {
        return Freecam.plugin;
    }
    
    public static void msg(final CommandSender sender, final String key) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Freecam.cfg.getString("messages." + key)));
    }
    
    public static void enableFreecam(final Player p) {
        p.updateInventory();
        Freecam.active_players.put(p, new PlayerData(p));
        p.setGameMode(GameMode.SPECTATOR);
        msg((CommandSender)p, "toggleOn");
        Freecam.active_tasks.put(p, Freecam.sch.runTaskLater(Freecam.plugin, () -> {
            if (Freecam.active_players.containsKey(p)) {
                disableFreecam(p);
                msg((CommandSender)p, "timeLimit");
            }
        }, (long)(Freecam.cfg.getInt("timeLimit") * 20)));
    }
    
    public static void disableFreecam(final Player p) {
        final PlayerData dat = Freecam.active_players.get(p);
        if (Freecam.npc != null) {
            Freecam.npc.removeNPC(dat.getNPCId());
        }
        p.teleport(dat.getLocation());
        p.setGameMode(GameMode.SURVIVAL);
        Freecam.active_players.remove(p);
        msg((CommandSender)p, "toggleOff");
        Freecam.active_tasks.get(p).cancel();
        Freecam.active_tasks.remove(p);
    }
    
    public static Location getLastLoc(final Player p) {
        return Freecam.active_players.get(p).getLocation();
    }
}
