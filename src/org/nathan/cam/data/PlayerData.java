package org.nathan.cam.data;

import org.bukkit.entity.*;
import java.util.*;
import org.bukkit.*;
import org.nathan.cam.Freecam;
import org.nathan.cam.utils.*;

public class PlayerData
{
    private Player player;
    private String uuid;
    private Location location;
    private long timestamp;
    private UUID npcid;
    
    public PlayerData(final String uuid, final String location, final long timestamp) {
        this.player = Bukkit.getPlayer(UUID.fromString(uuid));
        this.uuid = uuid;
        this.location = SerializationUtils.locationDeserialize(location);
        this.timestamp = timestamp;
    }
    
    public PlayerData(final Player player) {
        this.player = player;
        this.uuid = player.getUniqueId().toString();
        this.location = player.getLocation();
        this.timestamp = System.currentTimeMillis();
        if (Freecam.npc != null) {
            this.npcid = Freecam.npc.spawnNPC(player);
        }
    }
    
    public Player getPlayer() {
        return this.player;
    }
    
    public String getUuid() {
        return this.uuid;
    }
    
    public Location getLocation() {
        return this.location;
    }
    
    public String getLocationString() {
        return SerializationUtils.locationSerialize(this.location);
    }
    
    public long getTimestamp() {
        return this.timestamp;
    }
    
    public UUID getNPCId() {
        return this.npcid;
    }
}
