package org.nathan.cam.data;


import java.sql.*;
import java.util.logging.*;
import org.bukkit.entity.*;
import org.nathan.cam.Freecam;
import org.nathan.cam.datasqlite.Errors;

import java.util.*;
import org.bukkit.*;

public abstract class Storage
{
    public Freecam plugin;
    public Connection connection;
    public String table;
    
    public Storage(final Freecam plugin) {
        this.table = "freecam_players";
        this.plugin = plugin;
    }
    
    public abstract Connection getSQLConnection();
    
    public abstract boolean load();
    
    public boolean initialize() {
        this.connection = this.getSQLConnection();
        try {
            final PreparedStatement ps = this.connection.prepareStatement("SELECT * FROM " + this.table + " WHERE uuid = ?");
        }
        catch (SQLException ex) {
            this.plugin.getLogger().severe("Unable to retreive connection");
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
   
    public void drop() {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.getSQLConnection();
            ps = conn.prepareStatement("DELETE FROM `" + this.table + "`;");
            ps.executeUpdate();
        }
        catch (SQLException ex) {
            this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            }
            catch (SQLException ex2) {
                this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex2);
            }
            return;
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            }
            catch (SQLException ex2) {
                this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex2);
            }
        }
        try {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        catch (SQLException ex2) {
            this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex2);
        }
    }
    
    @SuppressWarnings("unused")
	public void put() {
        this.drop();
        for (final Map.Entry<Player, PlayerData> entry : Freecam.active_players.entrySet()) {
            final PlayerData dat = entry.getValue();
            Connection conn = null;
            PreparedStatement ps = null;
            try {
                conn = this.getSQLConnection();
                ps = conn.prepareStatement(String.format("INSERT INTO `" + this.table + "` ('uuid', 'location', 'timestamp') VALUES ('%s', '%s', %d);", dat.getUuid(), dat.getLocationString(), dat.getTimestamp()));
                ps.executeUpdate();
            }
            catch (SQLException ex) {
                this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
                try {
                    if (ps != null) {
                        ps.close();
                    }
                    if (conn == null) {
                        continue;
                    }
                    conn.close();
                }
                catch (SQLException ex2) {
                    this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex2);
                }
                continue;
            }
            finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                }
                catch (SQLException ex2) {
                    this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex2);
                }
            }
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn == null) {
                    continue;
                }
                conn.close();
            }
            catch (SQLException ex2) {
                this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex2);
            }
        }
    }
    
    public void get() {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = this.getSQLConnection();
            ps = conn.prepareStatement("SELECT * FROM `" + this.table + "`;");
            rs = ps.executeQuery();
            while (rs.next()) {
                Freecam.active_players.put(Bukkit.getPlayer(UUID.fromString(rs.getString("uuid"))), new PlayerData(rs.getString("uuid"), rs.getString("location"), rs.getLong("timestamp")));
            }
            this.drop();
        }
        catch (Exception ex) {
            this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            }
            catch (SQLException ex2) {
                this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex2);
            }
            return;
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            }
            catch (SQLException ex2) {
                this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex2);
            }
        }
        try {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        catch (SQLException ex2) {
            this.plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex2);
        }
    }
}
