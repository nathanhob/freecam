package org.nathan.cam.utils;

import java.util.*;
import net.citizensnpcs.api.*;
import net.citizensnpcs.api.npc.*;
import net.citizensnpcs.api.npc.NPC;

import org.bukkit.entity.*;
import org.bukkit.command.*;
import org.bukkit.event.*;
import org.nathan.cam.Freecam;

import net.citizensnpcs.api.event.*;

public class NPCUtils implements Listener
{
    private NPCRegistry registry;
    public Map<UUID, Player> active_npcs;
    
    public NPCUtils() {
        this.active_npcs = new HashMap<UUID, Player>();
        this.registry = CitizensAPI.getNPCRegistry();
        Freecam.bpm.registerEvents((Listener)this, Freecam.getPlugin());
    }
    
    public UUID spawnNPC(final Player p) {
        final NPC npc = this.registry.createNPC(EntityType.PLAYER, p.getName());
        npc.setProtected(false);
        npc.spawn(p.getLocation());
        if (npc.isSpawned()) {
            this.active_npcs.put(npc.getUniqueId(), p);
            return npc.getUniqueId();
        }
        return null;
    }
    
    public void removeNPC(final UUID uuid) {
        if (!this.active_npcs.containsKey(uuid)) {
            return;
        }
        this.registry.getByUniqueId(uuid).destroy();
        this.active_npcs.remove(uuid);
    }
    
    public boolean isFreecamNPC(final Entity e) {
        return e.hasMetadata("NPC") && this.active_npcs.containsKey(this.registry.getNPC(e).getUniqueId());
    }
    
    public boolean isFreecamNPC(final NPC npc) {
        return this.active_npcs.containsKey(npc.getUniqueId());
    }
    
    public Player getNPCPlayer(final Entity e) {
        return this.active_npcs.get(this.registry.getNPC(e).getUniqueId());
    }
    
    public Player getNPCPlayer(final UUID uuid) {
        return this.active_npcs.get(uuid);
    }
    
    @EventHandler
    public void onHit(final NPCDamageEvent e) {
        if (this.isFreecamNPC(e.getNPC())) {
            final Player p = this.getNPCPlayer(e.getNPC().getUniqueId());
            Freecam.disableFreecam(p);
            Freecam.msg((CommandSender)p, "npcHit");
        }
    }
    
    @EventHandler
    public void onHit(final NPCDamageByBlockEvent e) {
        if (this.isFreecamNPC(e.getNPC())) {
            final Player p = this.getNPCPlayer(e.getNPC().getUniqueId());
            Freecam.disableFreecam(p);
            Freecam.msg((CommandSender)p, "npcHit");
        }
    }
    
    @EventHandler
    public void onHit(final NPCDamageByEntityEvent e) {
        if (this.isFreecamNPC(e.getNPC())) {
            final Player p = this.getNPCPlayer(e.getNPC().getUniqueId());
            Freecam.disableFreecam(p);
            Freecam.msg((CommandSender)p, "npcHit");
        }
    }
    
    @EventHandler
    public void onPush(final NPCPushEvent e) {
        if (this.isFreecamNPC(e.getNPC())) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onDeath(final NPCDeathEvent e) {
        if (this.isFreecamNPC(e.getNPC())) {
            final Player p = this.getNPCPlayer(e.getNPC().getUniqueId());
            Freecam.disableFreecam(p);
            Freecam.msg((CommandSender)p, "npcHit");
            p.damage(99999.0);
        }
    }
}