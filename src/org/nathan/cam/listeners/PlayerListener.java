package org.nathan.cam.listeners;

import org.bukkit.event.*;
import org.bukkit.entity.*;
import java.util.*;
import org.bukkit.event.inventory.*;
import org.bukkit.event.block.*;
import org.bukkit.command.*;
import org.bukkit.event.player.*;
import org.nathan.cam.Freecam;

public class PlayerListener implements Listener
{
    @EventHandler
    public void onJoin(final PlayerJoinEvent e) {
        if (Freecam.active_players.containsKey(e.getPlayer())) {
            Freecam.disableFreecam(e.getPlayer());
        }
    }
    
    @EventHandler
    public void onQuit(final PlayerQuitEvent e) {
        if (Freecam.active_players.containsKey(e.getPlayer())) {
            Freecam.disableFreecam(e.getPlayer());
        }
    }
    
    @EventHandler
    public void onQuit(final PlayerKickEvent e) {
        if (Freecam.active_players.containsKey(e.getPlayer())) {
            Freecam.disableFreecam(e.getPlayer());
        }
    }
    
    @EventHandler
    public void onTeleport(final PlayerTeleportEvent e) {
        if (!e.getPlayer().isOp() && e.getCause().equals((Object)PlayerTeleportEvent.TeleportCause.SPECTATE)) {
            e.setCancelled(true);
            e.setTo(e.getFrom());
        }
        if (e.getCause().equals((Object)PlayerTeleportEvent.TeleportCause.COMMAND)) {
            for (final Player p : e.getPlayer().getWorld().getPlayers()) {
                if (p.getLocation().distanceSquared(e.getPlayer().getLocation()) <= Freecam.cfg.getDouble("maxDistance") * Freecam.cfg.getDouble("maxDistance") && Freecam.active_players.containsKey(p)) {
                    e.setCancelled(true);
                    e.setTo(e.getFrom());
                }
            }
        }
    }
    
    @EventHandler
    public void onClick(final InventoryClickEvent e) {
        if (Freecam.active_players.containsKey(e.getWhoClicked())) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onInteract(final PlayerInteractEvent e) {
        if (Freecam.active_players.containsKey(e.getPlayer())) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onPlace(final BlockPlaceEvent e) {
        if (Freecam.active_players.containsKey(e.getPlayer())) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onBreak(final BlockBreakEvent e) {
        if (Freecam.active_players.containsKey(e.getPlayer())) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onWorldChange(final PlayerChangedWorldEvent e) {
        if (Freecam.active_players.containsKey(e.getPlayer())) {
            Freecam.disableFreecam(e.getPlayer());
        }
    }
    
    @EventHandler
    public void onChat(final PlayerCommandPreprocessEvent e) {
        final String[] split = e.getMessage().split(" ");
        if (Freecam.active_players.containsKey(e.getPlayer()) && !split[0].equals("/freecam") && !split[0].equals("/fc") && !split[0].equals("/freeroam")) {
            e.setCancelled(true);
            Freecam.msg((CommandSender)e.getPlayer(), "noChat");
        }
    }
    
    @EventHandler
    public void onMove(final PlayerMoveEvent e) {
        if (e.getFrom().getWorld().equals(e.getTo().getWorld()) && e.getFrom().getBlockX() == e.getTo().getBlockX() && e.getFrom().getBlockY() == e.getTo().getBlockY() && e.getFrom().getBlockZ() == e.getTo().getBlockZ()) {
            return;
        }
        if (!Freecam.active_players.containsKey(e.getPlayer())) {
            return;
        }
        if (Freecam.getLastLoc(e.getPlayer()).distanceSquared(e.getTo()) > Freecam.cfg.getDouble("maxDistance") * Freecam.cfg.getDouble("maxDistance")) {
            e.setTo(e.getFrom());
            e.getPlayer().setVelocity(e.getPlayer().getVelocity().multiply(-20));
            Freecam.msg((CommandSender)e.getPlayer(), "maxDistance");
        }
    }
}
