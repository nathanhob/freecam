package org.nathan.cam.datasqlite;

import java.util.logging.*;

import org.nathan.cam.Freecam;
import org.nathan.cam.data.Storage;

import java.io.*;
import java.sql.*;

public class SQLite extends Storage
{
    String dbname;
    
    public SQLite(final Freecam instance) {
        super(instance);
        this.dbname = "data";
    }
    
    @Override
    public Connection getSQLConnection() {
        final File dataFolder = new File(this.plugin.getDataFolder(), String.valueOf(this.dbname) + ".db");
        if (!dataFolder.exists()) {
            try {
                dataFolder.createNewFile();
            }
            catch (IOException e) {
                this.plugin.getLogger().log(Level.SEVERE, "File write error: " + this.dbname + ".db");
            }
        }
        try {
            if (this.connection != null && !this.connection.isClosed()) {
                return this.connection;
            }
            Class.forName("org.sqlite.JDBC");
            return this.connection = DriverManager.getConnection("jdbc:sqlite:" + dataFolder);
        }
        catch (SQLException ex) {
            this.plugin.getLogger().log(Level.SEVERE, "SQLite exception on initialize", ex);
        }
        catch (ClassNotFoundException ex2) {
            this.plugin.getLogger().log(Level.SEVERE, "You need the SQLite JBDC library. Google it. Put it in /lib folder.");
        }
        return null;
    }
    
    @Override
    public boolean load() {
        this.connection = this.getSQLConnection();
        try {
            final Statement s = this.connection.createStatement();
            s.executeUpdate("CREATE TABLE IF NOT EXISTS " + this.table + " ( `uuid` VARCHAR NOT NULL , `location` VARCHAR NOT NULL , `timestamp` BIGINT NOT NULL , PRIMARY KEY (`uuid`));");
            s.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return this.initialize();
    }
}
