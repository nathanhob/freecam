package org.nathan.cam.datasqlite;

import java.util.logging.*;

import org.nathan.cam.Freecam;

public class Error
{
    public static void execute(final Freecam plugin, final Exception ex) {
        plugin.getLogger().log(Level.SEVERE, "Couldn't execute MySQL statement: ", ex);
    }
    
    public static void close(final Freecam plugin, final Exception ex) {
        plugin.getLogger().log(Level.SEVERE, "Failed to close MySQL connection: ", ex);
    }
}
