package org.nathan.cam.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.nathan.cam.Freecam;

public class FreecamCommand implements CommandExecutor
{
    public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
        if (!sender.hasPermission("freecam.use")) {
            Freecam.msg(sender, "noPermission");
            return false;
        }
        if (Freecam.active_players.containsKey(sender)) {
            Freecam.disableFreecam((Player)sender);
        }
        else {
            Freecam.enableFreecam((Player)sender);
        }
        return true;
    }
}
